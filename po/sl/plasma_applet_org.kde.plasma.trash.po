# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Andrej Mernik <andrejm@ubuntu.si>, 2014, 2015, 2016.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-18 01:01+0000\n"
"PO-Revision-Date: 2023-03-01 07:04+0100\n"
"Last-Translator: Matjaž Jeran <matjaz.jeran@amis.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"X-Generator: Poedit 3.2.2\n"

#: contents/ui/main.qml:100
#, kde-format
msgctxt "a verb"
msgid "Open"
msgstr "Odpri"

#: contents/ui/main.qml:101
#, kde-format
msgctxt "a verb"
msgid "Empty"
msgstr "Izprazni"

#: contents/ui/main.qml:107
#, kde-format
msgid "Trash Settings…"
msgstr "Nastavitev smeti…"

#: contents/ui/main.qml:158
#, kde-format
msgid ""
"Trash\n"
"Empty"
msgstr ""
"Smeti\n"
"prazne"

#: contents/ui/main.qml:158
#, kde-format
msgid ""
"Trash\n"
"One item"
msgid_plural ""
"Trash\n"
" %1 items"
msgstr[0] ""
"Smeti\n"
"%1 predmet"
msgstr[1] ""
"Smeti\n"
"%1 predmeta"
msgstr[2] ""
"Smeti\n"
"%1 predmeti"
msgstr[3] ""
"Smeti\n"
"%1 predmetov"

#: contents/ui/main.qml:167
#, kde-format
msgid "Trash"
msgstr "Smeti"

#: contents/ui/main.qml:168
#, kde-format
msgid "Empty"
msgstr "Prazno"

#: contents/ui/main.qml:168
#, kde-format
msgid "One item"
msgid_plural "%1 items"
msgstr[0] "%1 predmet"
msgstr[1] "%1 predmeta"
msgstr[2] "%1 predmeti"
msgstr[3] "%1 predmetov"

#~ msgid ""
#~ "Trash \n"
#~ " Empty"
#~ msgstr ""
#~ "Smeti \n"
#~ " prazne"

#~ msgid "Empty Trash"
#~ msgstr "Izprazni Smeti"

#~ msgid ""
#~ "Do you really want to empty the trash ? All the items will be deleted."
#~ msgstr "Ali resnično želite izprazniti Smeti? Vsi predmeti bodo izbrisani."

#~ msgid "Cancel"
#~ msgstr "Prekliči"
