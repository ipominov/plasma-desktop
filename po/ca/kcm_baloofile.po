# Translation of kcm_baloofile.po to Catalan
# Copyright (C) 2014-2022 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2014, 2017, 2019, 2020, 2021, 2022.
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019, 2020, 2021.
# Empar Montoro Martín <montoro_mde@gva.es>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-24 02:08+0000\n"
"PO-Revision-Date: 2022-12-14 09:02+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Generator: Lokalize 20.12.0\n"

#: ui/main.qml:50
#, kde-format
msgid "Pause Indexer"
msgstr "Posa en pausa l'indexador"

#: ui/main.qml:50
#, kde-format
msgid "Resume Indexer"
msgstr "Reprèn l'indexador"

#: ui/main.qml:77
#, kde-format
msgid ""
"This will disable file searching in KRunner and launcher menus, and remove "
"extended metadata display from all KDE applications."
msgstr ""
"Això desactivarà la cerca de fitxers en el KRunner i en els menús del "
"llançador, i eliminarà la visualització de metadades ampliades a totes les "
"aplicacions KDE."

#: ui/main.qml:86
#, kde-format
msgid ""
"Do you want to delete the saved index data? %1 of space will be freed, but "
"if indexing is re-enabled later, the entire index will have to be re-created "
"from scratch. This may take some time, depending on how many files you have."
msgstr ""
"Voleu suprimir les dades desades d'índex? S'alliberarà %1 d'espai, però si "
"més tard es reactiva la indexació, es tornarà a crear l'índex sencer des de "
"zero. Això pot trigar una estona, depenent de la quantitat de fitxers que hi "
"hagi."

#: ui/main.qml:88
#, kde-format
msgid "Delete Index Data"
msgstr "Suprimeix les dades d'índex"

#: ui/main.qml:108
#, kde-format
msgid "The system must be restarted before these changes will take effect."
msgstr "Cal reiniciar el sistema perquè aquests canvis tinguin efecte."

#: ui/main.qml:112
#, kde-format
msgctxt "@action:button"
msgid "Restart"
msgstr "Reinicia"

#: ui/main.qml:119
#, fuzzy, kde-format
#| msgid ""
#| "File Search helps you quickly locate all your files based on their "
#| "content."
msgid ""
"File Search helps you quickly locate your files. You can choose which "
"folders and what types of file data are indexed."
msgstr ""
"La cerca de fitxers us ajuda a localitzar ràpidament tots els fitxers basant-"
"se en el seu contingut."

#: ui/main.qml:134
#, fuzzy, kde-format
#| msgid "Currently indexing: %1"
msgctxt "@title:group"
msgid "File indexing:"
msgstr "Actualment s'està indexant: %1"

#: ui/main.qml:135
#, kde-format
msgctxt "@action:check"
msgid "Enabled"
msgstr ""

#: ui/main.qml:149
#, kde-format
msgctxt "@label indexing status"
msgid "Status:"
msgstr ""

#: ui/main.qml:153
#, fuzzy, kde-format
#| msgid "Status: %1, %2% complete"
msgid "%1, %2% complete"
msgstr "Estat: %1, %2% complet"

#: ui/main.qml:162
#, fuzzy, kde-format
#| msgid "Currently indexing: %1"
msgctxt "@label file currently being indexed"
msgid "Currently Indexing:"
msgstr "Actualment s'està indexant: %1"

#: ui/main.qml:165
#, fuzzy, kde-kuit-format
#| msgid "Currently indexing: %1"
msgctxt "@info"
msgid "Currently Indexing: <filename>%1</filename>"
msgstr "Actualment s'està indexant: %1"

#: ui/main.qml:181
#, kde-format
msgctxt "@title:group"
msgid "Data to index:"
msgstr ""

#: ui/main.qml:183
#, kde-format
msgid "File names and contents"
msgstr ""

#: ui/main.qml:196
#, kde-format
msgid "File names only"
msgstr ""

#: ui/main.qml:215
#, fuzzy, kde-format
#| msgid "Index hidden files and folders"
msgid "Hidden files and folders"
msgstr "Indexa els fitxers i carpetes ocults"

#: ui/main.qml:271
#, kde-format
msgctxt "@title:table Locations to include or exclude from indexing"
msgid "Locations"
msgstr ""

#: ui/main.qml:276
#, fuzzy, kde-format
#| msgid "Start indexing a folder…"
msgctxt "@action:button"
msgid "Start Indexing a Folder…"
msgstr "Inicia la indexació d'una carpeta…"

#: ui/main.qml:287
#, fuzzy, kde-format
#| msgid "Stop indexing a folder…"
msgctxt "@action:button"
msgid "Stop Indexing a Folder…"
msgstr "Atura la indexació d'una carpeta…"

#: ui/main.qml:349
#, kde-format
msgid "Not indexed"
msgstr "No indexada"

#: ui/main.qml:350
#, kde-format
msgid "Indexed"
msgstr "Indexada"

#: ui/main.qml:380
#, kde-format
msgid "Delete entry"
msgstr "Suprimeix l'entrada"

#: ui/main.qml:395
#, kde-format
msgid "Select a folder to include"
msgstr "Seleccioneu una carpeta a incloure"

#: ui/main.qml:395
#, kde-format
msgid "Select a folder to exclude"
msgstr "Seleccioneu una carpeta a excloure"

#~ msgid "Enable File Search"
#~ msgstr "Activa la cerca de fitxers"

#~ msgid "Also index file content"
#~ msgstr "Indexa també el contingut dels fitxers"

#~ msgid "Folder specific configuration:"
#~ msgstr "Configuració específica de carpeta:"
